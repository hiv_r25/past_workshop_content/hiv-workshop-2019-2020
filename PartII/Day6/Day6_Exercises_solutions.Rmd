---
title: "Quantitative Methods for HIV Researchers"
subtitle: "Part II Day 5 Exercises"
author: "Tina Davenport"
date: "November 15, 2019"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
Sys.setenv("LANGUAGE"="En")
Sys.setlocale("LC_ALL", "English")


#############################################################################
##                                                                         ##
##  Author: tina.davenport@duke.edu                                        ##
##  Program: Day3_Exercises_solutions                                      ##
##  Date: 11/15/2019                                                       ##
##  Purpose: This program performs some of the examples outined in the     ##
##           notes and gives solutions to class exercises and examples     ##
##                                                                         ##
##                                                                         ##
##  Change Log:                                                            ##
##  11/15/2019 File created                                                ##
##                                                                         ##
#############################################################################

setwd("~/hiv-workshop-materials2019/PartII/Day6/")
library(tidyverse)
library(survival)
library(cmprsk)
rm(list=ls())
```


# In-Class Examples and Exercises

### Slide 7
```{r}
PBC <- read.csv(file="PBCdat.csv", header=TRUE, na.strings="")
PBC$Statdich <- ifelse(PBC$Status==2, 1, 0)        # ignore TX for now
  # table(PBC$Status, PBC$Statdich, exclude=NULL)  # check!
# summary(PBC)
# dim(PBC)
```


### Slide 14
Kaplan-Meier by hand on toy example
```{r}
set.seed(1)
Toy <- rbind(PBC %>% filter(Statdich==1) %>% sample_n(6),  # sample 6 deaths
    PBC %>% filter(Statdich==0) %>% sample_n(4)  ) %>%  # and 4 censored obs
    select(CaseID, Time, Statdich) %>%                  # subset by cols
    arrange(Time)                                       # sort by time

# Add a couple of duplicate times
Toy <- rbind(Toy, data.frame(CaseID=c(-881, -882), Time=rep(Toy$Time[6], 2),
    Statdich=c(0,1))) %>% arrange(Time) 
Toy

n <- nrow(Toy)             # sample size
times <- unique(Toy$Time)  # unique event/censor times
CumuSurv <- 1              # cumulative survival prob at t0
KMest <- data.frame(t=0, AtRisk=n, CumuSurv)  # initialize KM estimator

for(t in times){  # t=223
    tmp <- Toy[which(Toy$Time==t),]   # individuals with time t
    AtRisk <- n                       # number currently at risk
    ndied <- sum(tmp$Statdich==1)     # number who died at time t
    ncens <- sum(tmp$Statdich!=1)     # number censored at time t
    CumuSurv <- CumuSurv*(n-ndied)/n  # KM estimate at time t
    KMest <- rbind(KMest, data.frame(t, AtRisk, CumuSurv))
    n <- n-ndied-ncens
}
rm(n, times, CumuSurv, t, AtRisk, ndied, ncens, tmp)  # clean up workspace
```


### Slide 16
Kaplan-Meier in R for toy example
```{r}
# create survival object - needed for fitting survival models
Sobj <- Surv(time=Toy$Time, event=Toy$Statdich)

# Get K-M estimates
KMestR <- survfit(Sobj~1)
# same estimates of survival probability:
# table(round(KMestR$surv,3)==round(KMest$CumuSurv[-1],3))
# KMest;  cbind(KMestR$time, KMestR$n.risk, KMestR$surv)

summary(KMestR)
```


### Slide 17-18
Inference and plot of Kaplan-Meier
```{r}
KMinfo <- data.frame(Time=KMestR$time, Nrisk=KMestR$n.risk,
    St=KMestR$surv)
KMinfo 
KMinfo$Time[min(which(KMinfo$St<=0.5))]  # median survival time
KMinfo$St[min(which(KMinfo$Time/365.25 >=5))]  # 5-year survival

# Plot K-M curve
plot(KMestR, lwd=3, col=1, cex=1.3, cex.lab=1.3, cex.axis=1.3, cex.main=1.5,
     main="Kaplan-Meier Estimated Survival Curve", xlab="Time (days)",
     ylab="Probability", ylim=c(0,1))
```


### Slide 19-20
Comparing Kaplan-Meier curves
```{r}
# Add treatment arm to Toy data; drop the two fake CaseIDs
Toy <- merge(Toy, subset(PBC, select=c(CaseID, TRT)), by="CaseID",
    all=FALSE)  # all=F only keeps those in both datasets

# create survival object - needed for fitting survival models
Sobj <- Surv(time=Toy$Time, event=Toy$Statdich)

# Get K-M estimates by trt group
KMestR <- survfit(Sobj~TRT, data=Toy)


plot(KMestR, lwd=3, col=1:2, cex=1.3, cex.lab=1.3, cex.axis=1.3,
    cex.main=1.5, main="Kaplan-Meier Estimated Survival Curve",
    xlab="Time (days)", ylab="Probability", ylim=c(0,1))
  legend("bottomleft", legend=levels(Toy$TRT), col=1:2, lty=1, lwd=3,
      bty="n", cex=1.2)
```


### Slide 22
Log-rank test
```{r}
LogRT <- survdiff(Sobj~TRT, data=Toy)
LogRT
```


### Slide 28
Unadjusted Cox proportional hazard model with PBC data
```{r}
rm(list=ls())
PBC <- read.csv(file="PBCdat.csv", header=TRUE, na.strings="")
PBC$Statdich <- ifelse(PBC$Status==2, 1, 0)  # ignore TX for now

# create the survival outcome
Sobj <- Surv(time=PBC$Time, event=PBC$Statdich)

Coxout <- coxph(Sobj~TRT, data=PBC, x=TRUE, y=TRUE)
summary(Coxout)
# plot(cox.zph(fit=Coxout));  abline(h=0, col=2)
```


### Slide 33
Adjusted Cox proportional hazard model with PBC data
```{r}
Coxadj <- coxph(Sobj~I(Age/365.25)+I(log(Albumin))+I(log(Bilirubin))+Edema+
    I(log(Prothtime))+TRT, data=PBC, x=TRUE, y=TRUE)
summary(Coxadj)  # round(coef(summary(Coxadj)),3)
```


### Slide 35
Testing proportional hazard assumption with Schoenfeld residuals
```{r}
CheckPH <- cox.zph(fit=Coxadj)
CheckPH

# plot them to visually inspect
par(mfrow=c(3,2))
for(ii in 1:length(coef(Coxout))){
    plot(CheckPH[ii], cex=1.3, cex.lab=1.3, cex.axis=1.3)
    abline(h=0, col=2)
}
```


### Slide 38
Testing proportional hazard assumption with interaction terms
```{r}
PBC$cTime <- PBC$Time-mean(PBC$Time)  # create centered time

CoxadjTST <- coxph(Sobj~I(Age/365.25)+I(log(Albumin))+I(log(Bilirubin))+
    Edema+I(log(Prothtime))+TRT+
    I(Age/365.25):cTime+I(log(Albumin)):cTime+I(log(Bilirubin)):cTime+
    Edema:cTime+I(log(Prothtime)):cTime+TRT:cTime, data=PBC, x=TRUE, y=TRUE)
round(coef(summary(CoxadjTST)),3)
```



### Slide 44
CICR methods with competing risks
```{r}
rm(list=ls())
PBC <- read.csv(file="PBCdat.csv", header=TRUE, na.strings="")
PBC$Statdich <- ifelse(PBC$Status==2, 1, 0)  # censor TX
PBC$Status_Tx <- ifelse(PBC$Status==1, 1, 0)  # Censor death


#----------------------------------  CIF  ----------------------------------#
CIFcom <- cuminc(ftime=PBC$Time, fstatus=PBC$Status, cencode="0")


#---------------------------------  Plots  ---------------------------------#
# plot(CIFcom)
plot(CIFcom$"1 1"$time, CIFcom$"1 1"$est, type="l", lty=3, lwd=4, col=1,
    ylim=c(0,1), cex=1.3, cex.lab=1.5, cex.axis=1.5, xlab="Time (days)",
    ylab="Probability", main="Cumlative Incident Functions")
  lines(CIFcom$"1 2"$time, CIFcom$"1 2"$est, lty=1, lwd=4, col=1)
  legend("topleft", legend=c("Transplant", "Death"), col=1, lty=c(3,1),
      lwd=3, bty="n", cex=1.2)

# Get K-M estimates
Sobj <- Surv(time=PBC$Time, event=PBC$Statdich)
KMestR <- survfit(Sobj~1)
Sobj_Tx <- Surv(time=PBC$Time, event=PBC$Status_Tx)
KMest_Tx <- survfit(Sobj_Tx~1)

plot(CIFcom$"1 1"$time, CIFcom$"1 1"$est, type="l", lty=3, lwd=4, col=1,
    ylim=c(0,1), cex=1.3, cex.lab=1.5, cex.axis=1.5, xlab="Time (days)",
    ylab="Probability", main="Cumlative Incident Functions")
  lines(CIFcom$"1 2"$time, CIFcom$"1 2"$est, lty=1, lwd=4, col=1)
  lines(KMestR$time, 1-KMestR$surv, lty=1, lwd=4, col=2)
  lines(KMest_Tx$time, 1-KMest_Tx$surv, lty=3, lwd=4, col=2)
  legend("topleft", legend=c("CIF-Transplant", "CIF-Death", "KM-Transplant",
      "KM-Death"), col=c(1,1,2,2), lty=c(3,1,3,1), lwd=3, bty="n", cex=1.2)  

  
#----------------------------  CIF death by TRT  ---------------------------#
tmp <- cuminc(ftime=PBC$Time, fstatus=PBC$Status, group=PBC$TRT, cencode="0")
tmp$Tests # test if CIF same across arms for each outcome
tmp <- tmp[grep(" 2", names(tmp))]  # subset to just death events

plot(tmp, lwd=3, ylim=c(0,1), cex=1.3, cex.lab=1.5, col=1:2, lty=1,
    cex.axis=1.5, xlab="Time (days)", ylab="Cumulative Incidence",
    main="CIF for Death by Treatment Arm", wh=c(0,2))
  box()
  legend("topleft", legend=levels(PBC$TRT), col=1:2, lty=1, lwd=3,
      bty="n", cex=1.3)
```



### Slide 45
Regression models with competing risks
```{r}
#-------------------------  Subdistribution model  -------------------------#
SubHM <- crr(ftime=PBC$Time, fstatus=PBC$Status,  # for death, failcode=2
    cov1=model.matrix(~I(Age/365.25)+I(log(Albumin))+I(log(Bilirubin))+Edema+
    I(log(Prothtime))+TRT, data=PBC)[,-1], failcode=2, cencode=0)
summary(SubHM)
SubHM <- crr(ftime=PBC$Time, fstatus=PBC$Status,  # for Tx, failcode=1
    cov1=model.matrix(~I(Age/365.25)+I(log(Albumin))+I(log(Bilirubin))+Edema+
    I(log(Prothtime))+TRT, data=PBC)[,-1], failcode=1, cencode=0)
summary(SubHM)
```



<br>
<br>
<br>

## In-Depth Exercises
Below are exercises to reinforce concepts taught in the lecture part of the day.
<br>


### Question 1

a. Use the Kaplan-Meier procedure to estimate the survival curve of the entire PBC data. Plot the curve.
```{r}
rm(list=ls())
PBC <- read.csv(file="PBCdat.csv", header=TRUE, na.strings="")
PBC$Statdich <- ifelse(PBC$Status==2, 1, 0)        # ignore TX for now

# Get the K-M estimates
Sobj <- Surv(time=PBC$Time, event=PBC$Statdich)
KMestR <- survfit(Sobj~1)
KMestR

# Plot K-M curve
plot(KMestR, lwd=3, col=1, cex=1.3, cex.lab=1.3, cex.axis=1.3, cex.main=1.5,
     main="Kaplan-Meier Estimated Survival Curve", xlab="Time (days)",
     ylab="Probability", ylim=c(0,1))
```

b. What is the median survival time of the whole cohort? What is the 10-year survival rate?
```{r}
KMinfo <- data.frame(Time=KMestR$time, Nrisk=KMestR$n.risk,
    St=KMestR$surv)
KMinfo$Time[min(which(KMinfo$St<=0.5))]  # median survival time
KMinfo$St[min(which(KMinfo$Time/365.25 >=10))]  # 5-year survival
```

c. Use the Kaplan-Meier procedure to estimate the survival curve of the entire PBC data, stratified by treatment. Plot the curves and perform a log-rank test to compare them.
```{r}
# estimate survival by treatment
KMestR <- survfit(Sobj~TRT, data=PBC)
KMestR

#plot the curves
plot(KMestR, lwd=3, col=1:2, cex=1.3, cex.lab=1.3, cex.axis=1.3,
    cex.main=1.5, main="Kaplan-Meier Estimated Survival Curve",
    xlab="Time (days)", ylab="Probability", ylim=c(0,1))
  legend("bottomleft", legend=levels(PBC$TRT), col=1:2, lty=1, lwd=3,
      bty="n", cex=1.2)

# compare with log-rank test
LogRT <- survdiff(Sobj~TRT, data=PBC)
LogRT
```



### Question 2

Repeat Question 1 using the ACTG data. Of interest is the comparison of the three treatments on the composite event outcome described below.

The dataset "ACTG_r25_TTE.csv" contains time-to-event data for 3 outcomes of interest.

Time_VF is time to virologic failue in weeks. Status_VF is the event status (0=censored, 1=event).

Time_Tol is time to drug discontinuation due to toxicity in weeks. Status_Tol is the event status (0=censored, 1=event, 2=competing risk).

Time_VFT is time to first event of either virologic failure or discontinuation in weeks, a composite variable of the two defined above. Status_VFT is the event status (0=censored, 1=discontinuation, 2=VF 4=competing risk). This is the PRIMARY OUTCOME. Combine Status_VFT values 1 and 2 as an "event".

a. First merge the TTE data with baseline ACTG data and combine the Status_VFT values.
```{r}
ACTGbl <- read.csv(file="ACTG_r25.csv", header=TRUE, na.strings="") %>%
    filter(week==0)
# dim(ACTGbl);  summary(ACTGbl)

ACTGtte <- read.csv(file="ACTG_r25_TTE.csv", header=TRUE, na.strings="")
# dim(ACTGtte);  summary(ACTGtte)

ACTG <- merge(ACTGbl, ACTGtte, by="ntisid", all=TRUE) 
# dim(ACTG);  summary(ACTG)

ACTG$StatusDich <- ACTG$Status_VFT
  ACTG$StatusDich[ACTG$StatusDich==2] <- 1
  ACTG$StatusDich[ACTG$StatusDich==4] <- 0
#  table(ACTG$Status_VFT, ACTG$StatusDich, exclude=NULL)  # check!
```


b. Use the Kaplan-Meier procedure to estimate and plot the survival curve.
```{r}
# Get the K-M estimates
Sobj <- Surv(time=ACTG$Time_VFT, event=ACTG$StatusDich)
KMestR <- survfit(Sobj~1)
# cbind(KMestR$time, KMestR$n.risk, KMestR$n.event, KMestR$n.censor,
#     KMestR$surv)

# Plot K-M curve
plot(KMestR, lwd=3, col=1, cex=1.3, cex.lab=1.3, cex.axis=1.3, cex.main=1.5,
     main="Kaplan-Meier Estimated Survival Curve", xlab="Time (days)",
     ylab="Probability", ylim=c(0,1))
```


c. Use the Kaplan-Meier procedure to estimate and plot the survival curve stratified by treatment. Perform a log-rank test to compare them.
```{r}
# estimate survival by treatment
KMestR <- survfit(Sobj~Arm, data=ACTG)

#plot the curves
plot(KMestR, lwd=3, col=c(1:2,4), cex=1.3, cex.lab=1.3, cex.axis=1.3,
    cex.main=1.5, main="Kaplan-Meier Estimated Survival Curve",
    xlab="Time (days)", ylab="Probability", ylim=c(0,1))
  legend("bottomleft", legend=levels(ACTG$Arm), col=c(1:2,4), lty=1, lwd=3,
      bty="n", cex=1.2)

# compare with log-rank test
LogRT <- survdiff(Sobj~Arm, data=ACTG)
LogRT
```




### Question 3

Fit a Cox proportional hazard model to assess the effects of treatment, adjusted for age, sex, and previous AIDS diagnosis. Be sure to check the proportional hazard assumption.
```{r}
#-----------------------------  fit the model  -----------------------------#
Coxadj <- coxph(Sobj~age+sex+aids_dx+Arm, data=ACTG, x=TRUE, y=TRUE)
summary(Coxadj)  # round(coef(summary(Coxadj)),3)


#--------------------------  Check PH assumption  --------------------------#
CheckPH <- cox.zph(fit=Coxadj)
CheckPH

# plot them to visually inspect
par(mfrow=c(3,2))
for(ii in 1:length(coef(Coxadj))){
    plot(CheckPH[ii], cex=1.3, cex.lab=1.3, cex.axis=1.3)
    abline(h=0, col=2)
}

# Test with interaction terms
ACTG$cTime_VFT <- ACTG$Time_VFT-mean(ACTG$Time_VFT)  # create centered time

CoxadjTST <- coxph(Sobj~age+sex+aids_dx+Arm+
    age:cTime_VFT+sex:cTime_VFT+aids_dx:cTime_VFT+Arm:cTime_VFT,
    data=ACTG, x=TRUE, y=TRUE)
round(coef(summary(CoxadjTST)),3)
```





### Bonus
a. Plot the CIF for the composite event, accounting for the competing events in the Status_VFT variable as described above (0=censored, 1=discontinuation or VF, 4=competing risk)
```{r}
ACTG$Status_VFTcr <- ACTG$Status_VFT
ACTG$Status_VFTcr[ACTG$Status_VFTcr==2] <- 1
# table(ACTG$Status_VFT, ACTG$Status_VFTcr)  # check!

#----------------------------------  CIF  ----------------------------------#
CIFcom <- cuminc(ftime=ACTG$Time_VFT, fstatus=ACTG$Status_VFTcr, cencode="0")


#---------------------------------  Plots  ---------------------------------#
# plot(CIFcom)
plot(CIFcom$"1 1"$time, CIFcom$"1 1"$est, type="l", lty=1, lwd=4, col=1,
    ylim=c(0,0.3), cex=1.3, cex.lab=1.5, cex.axis=1.5, xlab="Time (days)",
    ylab="Probability", main="Cumlative Incident Functions")
  lines(CIFcom$"1 4"$time, CIFcom$"1 4"$est, lty=3, lwd=4, col=2)
  legend("topleft", legend=c("Discontinuation or VF", "Other Event"),
      col=1:2, lty=c(1,3), lwd=3, bty="n", cex=1.2)


#----------------------------  CIF death by TRT  ---------------------------#
tmp <- cuminc(ftime=ACTG$Time_VFT, fstatus=ACTG$Status_VFTcr,
    group=ACTG$Arm, cencode="0")
tmp$Tests # test if CIF same across arms for each outcome
          # significant different for Event, but not for CR events

tmp <- tmp[grep(" 1", names(tmp))]  # subset to just disco/VF

plot(tmp, lwd=3, ylim=c(0,1), cex=1.3, cex.lab=1.5, col=c(1:2,4), lty=1,
    cex.axis=1.5, xlab="Time (days)", ylab="Cumulative Incidence",
    main="CIF for Disc or VF by Treatment Arm", wh=c(0,2))
  box()
  legend("topleft", legend=levels(ACTG$Arm), col=c(1:2,4), lty=1, lwd=3,
      bty="n", cex=1.2)  
```



a. Fit the subdistribution hazard model for treatment, adjusted for age, sex, and previous AIDS diagnosis.
```{r}
#-------------------------  Subdistribution model  -------------------------#
SubHM <- crr(ftime=ACTG$Time_VFT, fstatus=ACTG$Status_VFTcr,
    cov1=model.matrix(~age+sex+aids_dx+Arm, data=ACTG)[,-1], failcode=1,
    cencode=0)
summary(SubHM)
#SubHM <- crr(ftime=ACTG$Time_VFT, fstatus=ACTG$Status_VFTcr,
#    cov1=model.matrix(~age+sex+aids_dx+Arm, data=ACTG)[,-1], failcode=4,
#    cencode=0)
#summary(SubHM)
```



