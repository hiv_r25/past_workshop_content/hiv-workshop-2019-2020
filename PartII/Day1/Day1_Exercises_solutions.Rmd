---
title: "Quantitative Methods for HIV Researchers"
subtitle: "Part II Day 1 Exercises"
author: "Tina Davenport"
date: "October 11, 2019"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
Sys.setenv("LANGUAGE"="En")
Sys.setlocale("LC_ALL", "English")

#change for container
setwd("~/hiv-workshop-materials2019/PartII/Day1/")
library(tidyverse); library(vioplot)
```


# Brief In-Class Exercises

### Slide 39

In R, compute the order statistics, mean, and median of the 5 CD4 counts.
```{r}
yy <- c(194, 87, 385, 241, 37)  # create a vector of data
sort(yy)  # yy %>% sort()
mean(yy)  # yy %>% mean()
median(yy)  # yy %>% median()
```


<br>
Note that there is no built in R function (that I know of) that calculates the mode. You can use the function I've written below.

```{r}
Mode <- function(x){       # find mode; no built in R function
    tbl <- sort(table(x))  # USE AT YOUR OWN RISK!
    if(var(tbl)==0){ NA    # return NA if all appear equally
    } else { as.numeric(names(tbl)[which(tbl==max(tbl))])  } 
}

Mode(yy)  # yy %>% Mode()
```


<br>
Play around with this function for a bit. Create data that have one mode, two modes, missing values, etc, and see if returns what you would expect.



### Slide 52

In R, compute the variance and standard deviation of the 5 CD4 counts.
```{r}
yy <- c(194, 87, 385, 241, 37)  # create a vector of data
var(yy)  # yy %>% var()
sd(yy)  # yy %>% sd()
```


<br>
Use the `quantile` function to calculate the 25th and 75th percentile of the 5 CD4 counts. Use the `IQR` function to compute the IQR.
```{r}
yy <- c(194, 87, 385, 241, 37)  # create a vector of data
quantile(yy, probs=c(0.25, 0.75))  # yy %>% quantile(probs=c(0.25, 0.75))
IQR(yy)
```


<br>
Play around with the `type=` option in the `quantile` function to see how the different options give different quantiles of the data.
```{r}
quantile(yy, probs=c(0.25, 0.75), type=4)
quantile(yy, probs=c(0.25, 0.75), type=5)
quantile(yy, probs=c(0.25, 0.75), type=6)
quantile(yy, probs=c(0.25, 0.75), type=8)
quantile(yy, probs=c(0.25, 0.75), type=9)
```


### Slide 57

Create the bar plots for marital status using the data below.
```{r}
mar <- c(rep("Married",136), rep("Separated",18), rep("Divorced",40),
    rep("Single",89), rep("Widowed",17))
mar <- factor(mar, levels=c("Married", "Separated", "Divorced", "Single",
    "Widowed"))
dat <- data.frame(SID=sprintf("P%04d", 1:length(mar)), married=sample(mar))
```

Bar plot by counts

```{r}
cts <- table(mar)  # compute the counts
cts
xpos <- barplot(cts, beside=TRUE, cex.names=1.2, col="grey80",
    ylim=c(0,150), ylab="Frequency (N)", xlab="Marital Status", cex.lab=1.2,
    cex.axis=1.2, cex.main=1.5,
    main="Counts of Participants by Marital Status")  # create the bar plot
  text(xpos, y=cts+4, labels=cts, cex=1.2)            # add counts to top of bars
  
# ggplot(data.frame(cts), aes(x=mar, y=Freq)) + geom_bar(stat="identity")
```

Bar plot by percentages

```{r} 
pct <- round(prop.table(table(mar))*100,1)  # compute the percentages
pct
xpos <- barplot(pct, beside=TRUE, cex.names=1.2, col="grey80",
    ylim=c(0,100), ylab="Percent (%)", xlab="Marital Status", cex.lab=1.2,
    cex.axis=1.2, cex.main=1.5,
    main="Percentage of Participants by Marital Status")
  text(xpos, y=pct+2.5, labels=paste0(pct, "%"), cex=1.2)
  
# ggplot(data.frame(pct), aes(x=mar, y=Freq)) + geom_bar(stat="identity")
```



### Slide 61

(Already completed in Part I)

Create a histogram of sepal width both overall, and by species.
```{r}
# Overall
iris %>% ggplot(aes(x=Sepal.Width)) +
        geom_histogram(breaks=seq(2, 4.4, .2))

# By species
iris %>% ggplot(aes(x=Sepal.Width, fill=Species)) +
        geom_histogram(breaks=seq(2, 4.4, .2)) +
        facet_grid(Species ~ .)
```



### Slide 64

Create the stem and leaf diagram using the age data.
```{r}
dat <- c(58, 56, 50, 48, 66, 43, 71, 60, 78, 54,
         57, 45, 67, 53, 36, 70, 64, 65, 56, 57)
stem(dat)  # dat %>% stem()
```



### Slide 67

(Already completed in Part I)

Create a boxplot of sepal width both overall, and by species.
```{r}
# Overall
iris %>% ggplot(aes(y=Sepal.Width)) + geom_boxplot()

# By species
iris %>% ggplot(aes(x=Species, y=Sepal.Width, fill=Species)) + geom_boxplot()
```



### Slide 75

(Already completed in Part I)

Create a scatterplot of sepal width by petal width. Color each point by species.
```{r}
# Scatterplot overall
iris %>% ggplot(aes(x=Sepal.Width, y=Petal.Width)) + geom_point()

# Best fit line
#iris %>% ggplot(aes(x=Sepal.Width, y=Petal.Width)) + geom_point() +
#    geom_smooth(method='lm', formula=y~x)

# Points by color
iris %>% ggplot(aes(x=Sepal.Width, y=Petal.Width, color=Species)) +
    geom_point(aes(shape=Species))

# best fit line
iris %>% ggplot(aes(x=Sepal.Width, y=Petal.Width, color=Species)) + 
    geom_point(aes(shape=Species)) +
    geom_smooth(method="lm", formula="y~x")
```


<br>
<br>
<br>

## In-Depth Exercises
Below are exercises to reinforce concepts taught in the lecture part of the day.
<br>


### Question 1
The Jackson Heart Study (JHS, https://www.jacksonheartstudy.org/) is a prospective cohort study of 5306 black Americans 21-94 years of age at enrollment from the tricounty (Hinds, Madison, and Rankin) area of the Jackson, MS. The primary outcome of JHS is adjudicated cardiovascular events (hear failure, stroke, or myocardial infarction) and participants were follow for about 8 years to determine if they experienced an event. Baseline data were collected from 2000-2004 and included demographic information, survey responses, and clinical measurements.

Researchers at Duke used baseline survey responses from JHS to determine if there was an association between the amount of time since a participant's last routine physical and their perceived trust in healthcare providers. Those with missing data on measures of interested were excluded.

1. What is the research question of the Duke study?
2. What is the primary outcome of the Duke study? What is the primary exposure?
3. Is the Duke study prospective or retrospective?



### Question 2
Below is a excerpt from an article:
<br>

Background: Low potassium has been identified as a risk factor for type 2 diabetes. Low potassium could be a potentially modifiable risk factor, particularly for African Americans.

Objective: We sought to determine the effects of potassium chloride (KCl) supplements, at a commonly prescribed dose, on measures of glucose metabolism.

Design: Among African-American adults with prediabetes, we conducted a double-blinded pilot randomized controlled trial that compared the effects of 40 mEq K/d as KCl supplements with a
matching placebo on 2-h change in glucose AUC measured from oral-glucose-tolerance tests (OGTTs).

The principal investigator performed chart reviews of the electronic medical records of patients from participating Duke primary care clinics who had been seen in clinic in the previous 12 mo. To be eligible for this study, participants had to be African American by self-reported race, aged >=30 y, and have [prediabetes]. [Enrolled] participants underwent a 3 hour 75-g oral-glucose-tolerance test (OGTT). After completion of the OGTT, participants were...randomly assigned...to receive either KCl or placebo.

https://doi.org/10.3945/ajcn.117.161570


1. Is this a prospective or retrospective study?
2. Describe the population from which the sample was drawn.
3. What is the primary outcome of the study? What is the primary exposure?



### Question 3
Read in the "NHANES_hiv.csv" dataset. Be sure to incude `na.strings=TRUE` option to correctly handle missing values in the original dataset. Otherwise, for missing factors, R will read in blanks as a new factor level.

```{r}
NH <- read.csv(file="NHANES_hiv.csv", header=TRUE, na.strings="")
```


This is a modified dataset from the National Health and Nutrition Examination Survey (NHANES) data available publically for use. (The full, unedited 2015-2016 data are available for download: https://wwwn.cdc.gov/nchs/nhanes/ContinuousNhanes/Default.aspx?BeginYear=2015) Some variable definitions are below:

SEQN: participant ID number

sex: participant's biological sex; 1=male, 0=female

age: participant's age in years

race: participant's race/Hispanic origin; 1=Mexican American, 2=Other Hispanic, 3=Non-Hispanic White, 4=Non-Hispanic Black, 5=Other Race - Including Multi-Racial

educ: participant's education level; 1=Less than 9th grade, 2=9-11th grade, (Includes 12th grade with no diploma), 3=High school graduate/GED or equivalent, 4=Some college or AA degree, 5=College graduate or above

marital: participant's marital status; 1=Married, 2=Widowed, 3=Divorced, 4=Separated, 5=Never married, 6=Living with partner

SBP: participant's systolic blood pressure in mmHg

ACR: urine albumin to creatinine ration in mg/g

eGFR: participant's estimated glomerular filtration rate in mL/min/1.73m2, measured by the CKD-EPI creatinine equation

A1C: participant's hemoglobin A1C in % 

chlam: participant's result of a urine chlamydia test; 0=negative, 1=positive

herpes1 and herpes2: participant's result of herpes simplex virus type 1 and 2 test; 0=negative, 1=positive

HIV: participant's result of HIV-1, 2 combination test; 0=HIV-1/2 non-reactive, 1=HIV-1/2 reactive

Alcohol use in the past 12 months and in lifetime, smoking status, ever user of marijuana, ever user of cocaine/heroin/methamphetamine, number of hours per night participant sleeps on weekdays or workdays.

PHQsum:	participant's patient health questionnaire depression scale score  (range 0 to 27, where 0 is no depression and 27 is most severe depression)

Several measures of sexual history


1. Do the data appear to be "tidy"?

2. Assume these data are every individual in the population of interest. How many individuals are in the population? How many variables are in the data? How many individuals in the population had a reactive HIV test?
```{r}
NH %>% summarise(unique=n_distinct((SEQN)))  # how many individuals
dim_desc(NH)  # how many variables are in the data
NH %>% group_by(HIV) %>% summarise(count=n())  # how many had HIV positive test?

#length(unique(NH$SEQN))      # how many individuals
#dim(NH)                      # how many variables are in the data
#table(NH$HIV, exclude=NULL)  # how many had HIV positive test?
```


3. Take a simple random sample without replacement of size 200 from the population. Be sure to set a seed so that the results can be replicated. In your sample, how many individuals had a reactive HIV test?
```{r}
set.seed(7134)
NHsub <- NH %>% sample_n(size=200, replace=FALSE)
dim(NHsub)  # check
NHsub %>% group_by(HIV) %>% summarise(count=n())  # how many had HIV positive test?

#set.seed(7134)
#NHsub <- NH[sample(1:nrow(NH), 200, replace=FALSE),]
```

4. When the outcome is extremely rare (e.g., reactive HIV test), is it better to perform a cohort study or a case-control study?


5. Draw a sample of 200 from the population using a case-control study design. Do this by sampling all $m$ individuals with a reactive HIV test (cases), and $200-m$ individuals with a non-reactive test (controls).
```{r}
NHr <- NH %>% filter(HIV==1)  # those with reactive test (cases)

# sample from those with non-reactive test (controls)
NHnr <- NH %>% filter(HIV==0) %>% sample_n(size=200-nrow(NHr), replace=FALSE)

NHsubCC <- rbind(NHr, NHnr)  # combine the two datasets
NHsubCC %>% group_by(HIV) %>% summarise(count=n())  # check

#indR <- which(NH$HIV==1)   # index of those reactive
#indNR <- which(NH$HIV==0)  # index of those non-reactive
#NHsubCC <- NH[c(indR, sample(indNR, 200-length(indR), replace=FALSE)),]
```


6. Add a variable to the population dataset that categorizes age into four group: <18, 18 to <35, 35 to <55, and 55+. How many individuals are in each stratum?  
```{r}
NH$ageC <- cut(NH$age, breaks=c(min(NH$age), 18, 35, 55, max(NH$age)), include.lowest=TRUE, right=FALSE)
NH %>% group_by(ageC) %>% summarise(count=n())  # how many in each stratum?
```


7. Draw a sample of size 200 from the population using a stratified random sampling design. Do this by taking a simple random sample of size 50 from each age stratum (disproportionate stratification).
```{r}
NHsubSrat <- NH %>% group_by(ageC) %>% sample_n(size=50)
NHsubSrat %>% group_by(ageC) %>% summarise(count=n())  # check

# use split, apply, combine method
#Strata <- split(NH, list(NH$ageC))  # creates a list with 4 elements for the 4 strata
#samps <- lapply(Strata, function(x){ x[sample(1:nrow(x), 50, replace=FALSE),] } ) # apply
#NHsubSrat <- do.call(rbind, samps)   # combine
```



8. Draw a sample from the population using a stratified random sampling design. Do this by randomly selecting 10% of individuals from each age stratum (proportionate stratification).
```{r}
NHsubSrat <- NH %>% group_by(ageC) %>% sample_frac(0.1)
NH %>% group_by(ageC) %>% summarise(count=n())  # n's for pop'n
NHsubSrat %>% group_by(ageC) %>% summarise(count=n())  # n's for sample 

#samps <- lapply(Strata,
#    function(x){ x[sample(1:nrow(x), ceiling(0.1*nrow(x)), replace=FALSE),] } ) # apply
#NHsubSrat <- do.call(rbind, samps)  # combine
```


9. Randomly select 5 income groups and sample all individuals from each of the selected groups (one-stage cluster sampling).
```{r}
clusters <- sample(unique(NH$income), 5, replace=FALSE)  # select the clusters
NHclust <- NH[which(NH$income %in% clusters),]  # subset population by clusters
table(NHclust$income, exclude=NULL)             # check for the 5 selected clusters only
table(NH$income, exclude=NULL)                  # whole population income breakdown
```


10. Take a simple random sample of size 40 from each of the selected clusters (two-stage cluster sampling).
```{r}
NHsubClus <- NHclust %>% group_by(income) %>% sample_n(40)
NHsubClus %>% group_by(income) %>% summarise(count=n())  # check!

# use split, apply, combine method
#Clus <- split(NHclust, NHclust$income)  # creates list, 5 elements for 5 clusters
#samps <- lapply(Clus, function(x){ x[sample(1:nrow(x), 40, replace=FALSE),] } ) # apply
#NHsubClus <- do.call(rbind, samps)
```



### Question 4
Read in the "NHANES_hiv.csv" dataset (see above). Remove all individuals less than 21 years of age.
```{r}
NHall <- read.csv(file="NHANES_hiv.csv", header=TRUE, na.strings="")
NH <- NHall %>% filter(age >=21)  # NH <- NHall[which(NHall$age >=21),]
dim(NH)
```


1. Based on the descriptions of the first 10 variables above, for which variables are the mean and median meaningful values?
2. For each of the following variables, determine which plots would be appropriate visualizations. Then based on those plots, determine which measures of central location and spread are most appropriate. (Note that "none" is a possible answer.)
    a. SEQN
    b. sex
    c. age
    d. race
    e. marital
    f. ACR
    g. eGFR
    h. A1C
    i. HIV

```{r}
NHexplore <- NH %>% select(SEQN, sex, age, race, marital, ACR, eGFR, A1C, HIV)

#-------------------  Exploratory data analyses / plots  -------------------#
for(ii in 1:ncol(NHexplore)){
    xax <- na.omit(NHexplore[,ii])
    if(ii%%3==1){
        #dev.new()
        par(mfrow=c(3,3))
    }
    hist(xax, xlab=names(NHexplore)[ii], main="", cex.lab=1.2, cex.axis=1.2)
    plot(density(xax), lwd=3, cex.lab=1.2, cex.axis=1.2,
        xlab=names(NHexplore)[ii], main="")
    boxplot(xax, lwd=2, cex.lab=1.2, cex.axis=1.2, pch=21, cex=1.25,
        col="grey", bg="grey", xlab=names(NHexplore)[ii])
}
```


