{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Looping with Globs\n",
    "It can be tedious and error prone to individually specify the files we want to loop over.  Regular expressions, in the form of bash globs allow us to automatically select groups of files to loop over.\n",
    "\n",
    "## Shell Variables\n",
    "Assign the variables in this notebook."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "source pe_analysis_config.sh\n",
    "mkdir -p $TRIMMED $STAR_OUT"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This is appropriate in some situations, but often when there is a group of files that we want to work with, we can find a simple way to list these files without specifying them one-by-one.  For example, since the reads for each FASTQ are split across four lanes, we might want to analyze the data from all four lanes at once.  There are several ways that we can specify the FASTQs from all four lanes of FASTQ 21_2019:\n",
    "\n",
    "1. The easiest is to use the `*` wildcard, which matches any number of any characters (including zero), so we can match the FASTQs from all four lanes like this: `$RAW_FASTQS/21_2019*`\n",
    "2. `*` is easy and useful, but often it is better to be more specific, otherwise we might match something unintentionally. Since the only difference in the names between the lanes is the \"L001\", \"L002\", \"L003\", and \"L004\", we can narrow our match using `?`, which matches any single character: `21_2019_P_M1_S21_L00?_R1_001.fastq.gz`\n",
    "3. The best approach is to specify exactly what characters we are allowing in that variable position.  Square brackets allow you to list specific characters that can match `[1234]` or a range `[1-4]`: `21_2019_P_M1_S21_L00[1-4]_R1_001.fastq.gz`\n",
    "\n",
    "Globs can be confusing.  Keep in mind that here we are using the globs to search through all the file names in a directory and list the files with a name that matches a specific pattern."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ls $RAW_FASTQS/10_H*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ls $RAW_FASTQS/10_H_S8_L00?_R1_001.fastq.gz"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ls $RAW_FASTQS/10_H_S8_L001_R?_001.fastq.gz"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## More Complex Globs"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can combine multiple wildcards in a glob to match a more complex set of filenames.  For example we could match samples 10 through 19, Lanes 1 through 4, for R1 with the following glob."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "ls $RAW_FASTQS/1?_*_L00[1-4]_R1_001.fastq.gz"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Looping over a glob\n",
    "Now we can put together `for` loops with globs, to loop over all the lanes from library 21_2019"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for FASTQ in $RAW_FASTQS/10_H_S8_L001_R?_001.fastq.gz\n",
    "    do\n",
    "        echo \"RUNNING FASTQ: ${FASTQ}\"\n",
    "    done"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## String Manipulation\n",
    "\n",
    "But we still have a bit of work to do.  Before when we were manually specifying each FASTQ, we looped over a *substring* of the full path, then added on the prefix and suffix, for example: `$TRIMMED/${FASTQ}_001.trim.fastq.gz`.  But now we are grabbing the full path, and we need to manipulate it so that we can generate output file names that are different than the input, and put the output in different directories.  We can do all this with the `basename` bash function. The simple form of `basename` removes the whole directory portion of a path and just returns the filename:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for FASTQ in $RAW_FASTQS/10_H_S8_L001_R?_001.fastq.gz\n",
    "    do\n",
    "        echo \"FULL PATH IS: ${FASTQ}\"\n",
    "        echo \"basename gives us: $(basename ${FASTQ})\"\n",
    "        echo \"\"\n",
    "    done"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you give `basename` a string as a second argument, it will strip that string from the end of the path (if it is there):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "for FASTQ in $RAW_FASTQS/10_H_S8_L001_R?_001.fastq.gz\n",
    "    do\n",
    "        echo \"FULL PATH IS: ${FASTQ}\"\n",
    "        echo \"basename gives us: $(basename ${FASTQ} '_001.fastq.gz')\"\n",
    "        echo \"\"\n",
    "    done"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "for FASTQ in $RAW_FASTQS/10_H_S8_L00?_R1_001.fastq.gz\n",
    "    do\n",
    "        echo \"FULL PATH IS: ${FASTQ}\"\n",
    "        echo \"basename gives us: $(basename ${FASTQ} '_R1_001.fastq.gz')\"\n",
    "        echo \"\"\n",
    "    done"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> Note that if the string you give is not a suffix of the path, nothing is stripped from the *end* of the path, but the directory prefix will still be removed:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Working with paired-end data adds a complication to the globy loop: we don't just want to iterate over all the FASTQs, we want to iterate over *pairs* of FASTQs."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "for FASTQ in $RAW_FASTQS/10_H_S8_L00?_R1_001.fastq.gz\n",
    "    do\n",
    "        echo \"FULL PATH IS: ${FASTQ}\"\n",
    "        FASTQ_BASE=\"$(basename ${FASTQ} '_R1_001.fastq.gz')\"\n",
    "        echo \"basename gives us: $FASTQ_BASE\"\n",
    "        echo \"OUTPUT PATH: ${TRIMMED}/${FASTQ_BASE}_R1_001.trim.fastq.gz\"\n",
    "        echo \"OUTPUT PATH: ${TRIMMED}/${FASTQ_BASE}_R2_001.trim.fastq.gz\"\n",
    "        echo \"-------------------------\"\n",
    "    done"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "With globs and `basename` in our toolbox, we are ready to **conquer the world** or at least run multiple FASTQs through our pipeline, without breaking a sweat!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## A globy pipeline"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for FASTQ in $RAW_FASTQS/10_H_S8_L00[1]_R1_001.fastq.gz\n",
    "    do\n",
    "        FASTQ_BASE=\"$(basename ${FASTQ} '_R1_001.fastq.gz')\"\n",
    "        echo \"---------------- TRIMMING: $FASTQ_BASE ----------------\"\n",
    "        fastq-mcf \\\n",
    "            $MYINFO/neb_e7600_adapters.fasta \\\n",
    "            $RAW_FASTQS/${FASTQ_BASE}_R1_001.fastq.gz \\\n",
    "            $RAW_FASTQS/${FASTQ_BASE}_R2_001.fastq.gz \\\n",
    "            -q 20 -x 0.5 \\\n",
    "            -o $TRIMMED/${FASTQ_BASE}_R1_001.trim.fastq.gz \\\n",
    "            -o $TRIMMED/${FASTQ_BASE}_R2_001.trim.fastq.gz\n",
    "        \n",
    "        echo \"---------------- MAPPING: $FASTQ_BASE ----------------\"\n",
    "        STAR \\\n",
    "            --runMode alignReads \\\n",
    "            --twopassMode None \\\n",
    "            --genomeDir $GENOME_DIR \\\n",
    "            --readFilesIn $TRIMMED/${FASTQ_BASE}_R1_001.trim.fastq.gz $TRIMMED/${FASTQ_BASE}_R2_001.trim.fastq.gz \\\n",
    "            --readFilesCommand gunzip -c \\\n",
    "            --outFileNamePrefix ${STAR_OUT}/${FASTQ_BASE}_ \\\n",
    "            --quantMode GeneCounts \\\n",
    "            --outSAMtype BAM SortedByCoordinate \\\n",
    "            --runThreadN $THREADS \\\n",
    "            --alignIntronMax 5000 \\\n",
    "            --outSJfilterIntronMaxVsReadN 500 1000 2000\n",
    "            \n",
    "    done"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### And let's check the result"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ls ${STAR_OUT}"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Bash",
   "language": "bash",
   "name": "bash"
  },
  "language_info": {
   "codemirror_mode": "shell",
   "file_extension": ".sh",
   "mimetype": "text/x-sh",
   "name": "bash"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
