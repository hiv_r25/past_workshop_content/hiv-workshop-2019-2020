---
title: "Quantitative Methods for HIV Researchers"
subtitle: "Part II Day 1 Exercises"
author: "Tina Davenport"
date: "October 11, 2019"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
Sys.setenv("LANGUAGE"="En")
Sys.setlocale("LC_ALL", "English")

#change for container
setwd("~/hiv-workshop-materials2019/PartII/Day1/")
library(tidyverse); library(vioplot)
```


# Brief In-Class Exercises

### Slide 39

In R, compute the order statistics, mean, and median of the 5 CD4 counts.
```{r}

```


<br>
Note that there is no built in R function (that I know of) that calculates the mode. You can use the function I've written below.

```{r}
Mode <- function(x){       # find mode; no built in R function
    tbl <- sort(table(x))  # USE AT YOUR OWN RISK!
    if(var(tbl)==0){ NA    # return NA if all appear equally
    } else { as.numeric(names(tbl)[which(tbl==max(tbl))])  } 
}
```


<br>
Play around with this function for a bit. Create data that have one mode, two modes, missing values, etc, and see if returns what you would expect.



### Slide 52

In R, compute the variance and standard deviation of the 5 CD4 counts.
```{r}

```


<br>
Use the `quantile` function to calculate the 25th and 75th percentile of the 5 CD4 counts. Use the `IQR` function to compute the IQR.
```{r}

```


<br>
Play around with the `type=` option in the `quantile` function to see how the different options give different quantiles of the data.
```{r}

```


### Slide 57

Create the bar plots for marital status using the data below.
```{r}
mar <- c(rep("Married",136), rep("Separated",18), rep("Divorced",40),
    rep("Single",89), rep("Widowed",17))
mar <- factor(mar, levels=c("Married", "Separated", "Divorced", "Single",
    "Widowed"))
dat <- data.frame(SID=sprintf("P%04d", 1:length(mar)), married=sample(mar))
```

Bar plot by counts

```{r}

```

Bar plot by percentages

```{r} 

```



### Slide 61

(Already completed in Part I)

Create a histogram of sepal width both overall, and by species.
```{r}

```



### Slide 64

Create the stem and leaf diagram using the age data.
```{r}

```



### Slide 67

(Already completed in Part I)

Create a boxplot of sepal width both overall, and by species.
```{r}

```



### Slide 75

(Already completed in Part I)

Create a scatterplot of sepal width by petal width. Color each point by species.
```{r}

```


<br>
<br>
<br>

## In-Depth Exercises
Below are exercises to reinforce concepts taught in the lecture part of the day.
<br>


### Question 1
The Jackson Heart Study (JHS, https://www.jacksonheartstudy.org/) is a prospective cohort study of 5306 black Americans 21-94 years of age at enrollment from the tricounty (Hinds, Madison, and Rankin) area of the Jackson, MS. The primary outcome of JHS is adjudicated cardiovascular events (hear failure, stroke, or myocardial infarction) and participants were follow for about 8 years to determine if they experienced an event. Baseline data were collected from 2000-2004 and included demographic information, survey responses, and clinical measurements.

Researchers at Duke used baseline survey responses from JHS to determine if there was an association between the amount of time since a participant's last routine physical and their perceived trust in healthcare providers. Those with missing data on measures of interested were excluded.

1. What is the research question of the Duke study?
2. What is the primary outcome of the Duke study? What is the primary exposure?
3. Is the Duke study prospective or retrospective?



### Question 2
Below is a excerpt from an article:
<br>

Background: Low potassium has been identified as a risk factor for type 2 diabetes. Low potassium could be a potentially modifiable risk factor, particularly for African Americans.

Objective: We sought to determine the effects of potassium chloride (KCl) supplements, at a commonly prescribed dose, on measures of glucose metabolism.

Design: Among African-American adults with prediabetes, we conducted a double-blinded pilot randomized controlled trial that compared the effects of 40 mEq K/d as KCl supplements with a
matching placebo on 2-h change in glucose AUC measured from oral-glucose-tolerance tests (OGTTs).

The principal investigator performed chart reviews of the electronic medical records of patients from participating Duke primary care clinics who had been seen in clinic in the previous 12 mo. To be eligible for this study, participants had to be African American by self-reported race, aged >=30 y, and have [prediabetes]. [Enrolled] participants underwent a 3 hour 75-g oral-glucose-tolerance test (OGTT). After completion of the OGTT, participants were...randomly assigned...to receive either KCl or placebo.

https://doi.org/10.3945/ajcn.117.161570


1. Is this a prospective or retrospective study?
2. Describe the population from which the sample was drawn.
3. What is the primary outcome of the study? What is the primary exposure?



### Question 3
Read in the "NHANES_hiv.csv" dataset. Be sure to incude `na.strings=TRUE` option to correctly handle missing values in the original dataset. Otherwise, for missing factors, R will read in blanks as a new factor level.

```{r}
NH <- read.csv(file="NHANES_hiv.csv", header=TRUE, na.strings="")
```


This is a modified dataset from the National Health and Nutrition Examination Survey (NHANES) data available publically for use. (The full, unedited 2015-2016 data are available for download: https://wwwn.cdc.gov/nchs/nhanes/ContinuousNhanes/Default.aspx?BeginYear=2015) Some variable definitions are below:

SEQN: participant ID number

sex: participant's biological sex; 1=male, 0=female

age: participant's age in years

race: participant's race/Hispanic origin; 1=Mexican American, 2=Other Hispanic, 3=Non-Hispanic White, 4=Non-Hispanic Black, 5=Other Race - Including Multi-Racial

educ: participant's education level; 1=Less than 9th grade, 2=9-11th grade, (Includes 12th grade with no diploma), 3=High school graduate/GED or equivalent, 4=Some college or AA degree, 5=College graduate or above

marital: participant's marital status; 1=Married, 2=Widowed, 3=Divorced, 4=Separated, 5=Never married, 6=Living with partner

SBP: participant's systolic blood pressure in mmHg

ACR: urine albumin to creatinine ration in mg/g

eGFR: participant's estimated glomerular filtration rate in mL/min/1.73m2, measured by the CKD-EPI creatinine equation

A1C: participant's hemoglobin A1C in % 

chlam: participant's result of a urine chlamydia test; 0=negative, 1=positive

herpes1 and herpes2: participant's result of herpes simplex virus type 1 and 2 test; 0=negative, 1=positive

HIV: participant's result of HIV-1, 2 combination test; 0=HIV-1/2 non-reactive, 1=HIV-1/2 reactive

Alcohol use in the past 12 months and in lifetime, smoking status, ever user of marijuana, ever user of cocaine/heroin/methamphetamine, number of hours per night participant sleeps on weekdays or workdays.

PHQsum:	participant's patient health questionnaire depression scale score  (range 0 to 27, where 0 is no depression and 27 is most severe depression)

Several measures of sexual history


1. Do the data appear to be "tidy"?

2. Assume these data are every individual in the population of interest. How many individuals are in the population? How many variables are in the data? How many individuals in the population had a reactive HIV test?
```{r}

```


3. Take a simple random sample without replacement of size 200 from the population. Be sure to set a seed so that the results can be replicated. In your sample, how many individuals had a reactive HIV test?
```{r}

```

4. When the outcome is extremely rare (e.g., reactive HIV test), is it better to perform a cohort study or a case-control study?


5. Draw a sample of 200 from the population using a case-control study design. Do this by sampling all $m$ individuals with a reactive HIV test (cases), and $200-m$ individuals with a non-reactive test (controls).
```{r}

```


6. Add a variable to the population dataset that categorizes age into four group: <18, 18 to <35, 35 to <55, and 55+. How many individuals are in each stratum?  
```{r}

```


7. Draw a sample of size 200 from the population using a stratified random sampling design. Do this by taking a simple random sample of size 50 from each age stratum (disproportionate stratification).
```{r}

```



8. Draw a sample from the population using a stratified random sampling design. Do this by randomly selecting 10% of individuals from each age stratum (proportionate stratification).
```{r}

```


9. Randomly select 5 income groups and sample all individuals from each of the selected groups (one-stage cluster sampling).
```{r}

```


10. Take a simple random sample of size 40 from each of the selected clusters (two-stage cluster sampling).
```{r}

```



### Question 4
Read in the "NHANES_hiv.csv" dataset (see above). Remove all individuals less than 21 years of age.
```{r}

```


1. Based on the descriptions of the first 10 variables above, for which variables are the mean and median meaningful values?
2. For each of the following variables, determine which plots would be appropriate visualizations. Then based on those plots, determine which measures of central location and spread are most appropriate. (Note that "none" is a possible answer.)
    a. SEQN
    b. sex
    c. age
    d. race
    e. marital
    f. ACR
    g. eGFR
    h. A1C
    i. HIV

```{r}

```


