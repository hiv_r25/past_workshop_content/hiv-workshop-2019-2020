---
title: "Quantitative Methods for HIV Researchers"
subtitle: "Ggplot Exercises and Solutions"
author: "Janice M. McCarthy"
date: "September 13 2019"
output: html_document
---

```{r}
library(tidyverse)
```
# R Graphics

## Exercises

1. Plot a scatter plot of petal width vs petal length from the iris data set. (Recall that in base R, you can access the column for petal width using iris$Petal.Width.)


2. Add a best fit line to the plot.


3. Create a box plot of petal width by species and another box plot of petal length by species. Make these plots sit side-by-side.


4. Create a scatter plot of petal width vs length and color/change symbol by species.



5. Add best fit lines for each species to this plot. 


6. Save this scatter plot to  a file called "figs/iris_ggplot.png".


7. Read the plot file back in and display in the markdown.

8. Create a histogram of Petal Length, and fill the bars by Species

9. Create box plots of Sepal Width by Species


## Great ggplot resource!

http://r-statistics.co/Top50-Ggplot2-Visualizations-MasterList-R-Code.html