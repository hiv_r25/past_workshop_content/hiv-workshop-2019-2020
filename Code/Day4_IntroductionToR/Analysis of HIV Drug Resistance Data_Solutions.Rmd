---
title: "Quantitative Methods for HIV Researchers"
subtitle: "Analysis of HIV Drug Resistance Data"
author: "Janice M. McCarthy"
date: "August 27, 2019"
output: html_document
---

```{r, setup, echo = FALSE, message=FALSE, warning=FALSE, root.dir = "~/hiv-workshop-materials2019/Code/Day4_IntroductionToR/"}
library(tidyverse)
library(stringr)
library(knitr)
```
# Hands-on Example

We are going to do a little exploratory data analysis with an HIV data set from Stanford. This will provide more practice and along the way, we will pick up some other R features such as `map` and making pretty tables.\\\\

This example is from the vignette of an R package called <a href = " https://CRAN.R-project.org/package=knockoff">'knockoff'</a>. I've translated their base R code into tidyverse. The italicized text below is a direct quote from that resource.

<i>
<p>In this vignette, we illustrate the analysis of a real (non-simulated) data set, including all the pre-processing steps. We hope this example will guide the user in their own applications of the knockoff filter. Throughout this example we will apply the knockoff filter in the Fixed-X scenario.</p>
<p>The scientific goal is to determine which mutations of the Human Immunodeficiency Virus Type 1 (HIV-1) are associated with drug resistance. The data set, publicly available from the <a href="http://hivdb.stanford.edu/pages/published_analysis/genophenoPNAS2006/">Stanford HIV Drug Resistance Database</a>, was originally analyzed in <span class="citation">(Rhee et al. 2006)</span>. The analysis described here is exactly that carried out for the knockoff filter paper <span class="citation">(Barber and Candes 2014)</span>.</p>
<div id="preparing-the-data" class="section level1">
<h1>Preparing the data</h1>
<p>The data set consists of measurements for three classes of drugs: protease inhibitors (PIs), nucleoside reverse transcriptase (RT) inhibitors (NRTIs), and nonnucleoside RT inhibitors (NNRTIs). Protease and reverse transcriptase are two enzymes in HIV-1 that are crucial to the function of the virus. This data set seeks associations between mutations in the HIV-1 protease and drug resistance to different PI type drugs, and between mutations in the HIV-1 reverse transcriptase and drug resistance to different NRTI and NNRTI type drugs.</p>
<p>In order to evaluate our results, we compare with the treatment-selected mutation panels created by <span class="citation">(Rhee et al. 2005)</span>. These panels give lists of HIV-1 mutations appearing more frequently in patients who have previously been treated with PI, NRTI, or NNRTI type drugs, than in patients with no previous exposure to that drug type. Increased frequency of a mutation among patients treated with a certain drug type implies that the mutation confers resistance to that drug type.</p>
<p>In this vignette, we will confine our attention to the PI drugs.</p></i>

```{r}
drug_class <- "PI"
```
<i>

## Fetching and cleaning the data
<p>First, we download the data and read it into a data frame.</p></i>

```{r}
base_url <- 'http://hivdb.stanford.edu/pages/published_analysis/genophenoPNAS2006'
gene_url <-  paste(base_url, 'DATA', paste0(drug_class, '_DATA.txt'), sep='/')

gene_df = read_tsv(gene_url)

gene_df 

```
In the gene data frame, the first two columns just identify the isolates (from in-vitro resistance measures taken from the blood of HIV patients). The next seven columns after the Medline ID are the resistance measures for each protease inhibitor. The columns P1 through P99 indicate mutaions (from a consensus sequence).\\\\

Let's summarize the types of mutations - subsitutions, insertions, deletions and WT. Insertions are detectable because the length of the code is greater than one. We'll use some string handling tools available in the `stringr` package.\\\\

We'll start by classifying just one position:

```{r}
get_mutant_type <- function(x){
  ifelse(x == "-", "deletion", ifelse(x == ".", "WT", ifelse(str_length(x) > 1, x == "insertion", ifelse((x %in% LETTERS),"substitution", NA))))
}

gene_df %>% mutate(MutationType = get_mutant_type(P10))
```
It's hard to see if this is correct, so let's just look at P10 and the result:



```{r}
gene_df %>% mutate(MutationType = get_mutant_type(P10)) %>% dplyr::select(P10, MutationType)
```
Oops! There is an error. Also, the function is a bit clunky. Let's switch to a nice `dplyr` function:

```{r}
get_mutant_type <- function(x){
  case_when(x == "-" ~ "deletion",
            x == "." ~  "WT",
            str_length(x) > 1 ~ "insertion",
            x %in% LETTERS ~ "substitution",
            TRUE ~ "NA")
}

gene_df %>% mutate(MutationType = get_mutant_type(P10)) %>% dplyr::select(P10, MutationType)
```

Ok. We have working code. Let's get this working for all the sites.

```{r}
get_mutant_type <- function(x){
  case_when(x == "-" ~ "deletion",
            x == "." ~  "WT",
            str_length(x) > 1 ~ "insertion",
            x %in% LETTERS ~ "substitution",
            TRUE ~ "NA")
}

gene_df %>% mutate_at(vars(matches("P")), .funs = get_mutant_type)
```
Oops again. We have other column names that have a "P" in them. Do we care? Not entirely, if our aim is to simply summarize the mutation types. But it's pretty sloppy. Let's make it neater.\\\\

```{r}
gene_df %>% mutate_at(vars(matches("P[1-99]")), .funs = get_mutant_type) -> gene_df_with_categories

gene_df_with_categories
```
Exercises:

1. How many isolates contain deletions at P95?

```{r}
# Base R
sum(gene_df_with_categories$P95 == "deletion")

```


2. How many isolates are there for each category of mutation in P95?

```{r}
gene_df_with_categories %>% dplyr::select(P95) %>% group_by(P95) %>% summarise(n = n())

gene_df_with_categories %>% dplyr::select(P95) %>% group_by(P95) %>% table()
```

Suppose we want to count the number of isolates with deletions in each site. This requires applying a function to the colummns of our data frame. We can use the function `map` from the `purrr` package. Experienced R users may be familiar with `lapply`. The mapping functions in `purrr` are 'cleaned up' versions of the apply family. 


```{r}
gene_df_with_categories %>% dplyr::select(matches("P[1-99]")) %>% map(function(x) sum(x=="deletion")) %>% .[1:5]
```
This returns a 'named list', which is nice for some things, but viewing is better with tibbles. Luckily, that's easy to fix.

```{r}
gene_df_with_categories %>% dplyr::select(matches("P[1-99]")) %>% map(function(x) sum(x=="deletion")) %>% as.tibble()
```
If we want to see all counts for all sites, we can do the following:

```{r}
gene_df_with_categories %>% dplyr::select(matches("P[1-99]")) %>% map(function(x) table(x)) %>% .[1:5]
``` 
But the table output is pretty ugly.

```{r}
gene_df_with_categories %>% dplyr::select(matches("P[1-99]")) %>% 
  map(function(x) c(sum(x=="insertion"),sum(x=="deletion"), sum(x == "substitution"), sum(x=="WT"))) %>% 
  as.tibble() -> mutation_count_df

# note - column names on tibbles aren't a thing anymore

add_column(mutation_count_df, MutationType = c("insertion", "deletion", "substitution", "WT"))

``` 
I really want the names in the first column, so I'll have a normal-looking table.
```{r}
add_column(mutation_count_df, MutationType = c("insertion", "deletion", "substitution", "WT"),.before = "P1") -> mutation_count_df

```
Take a moment and look at the knitted output in html. It's not how we would want to see this in a report. We can make the output much nicer using the function `kable` from the `knitr` package.

```{r}
mutation_count_df %>% kable()
```
It looks ugly in the viewer, but the knitted output is better. It's still a really wide table though. Because we are knitting to html, we can add features you wouldn't have in a word document or a pdf. For example, we can add sliders.

```{r}
#install.packages("kableExtra")
library(kableExtra)
mutation_count_df %>% kable %>%
  kable_styling() %>%
  scroll_box(width = "800px", height = "300px")
```
This table might be easier to read if we transpose it.

```{r}
 mutation_count_df %>% gather(Site, Count, P1:P99) %>% spread(MutationType, Count) 
```
This is not ideal, because the sites are now sorted alphabetically, and a bar plot of where mutations are would be more interesting if the sites were in order. We can use the `extract` function together with a regular expression:


```{r}
mutation_count_df %>% gather(Site, Count, P1:P99) %>% spread(MutationType, Count) %>% 
  extract(Site, c("A","B"), "(^[A-Z])([0-9]*)")
```

Exercise: Sort the data by site.

```{r}
mutation_count_df %>% gather(Site, Count, P1:P99) %>% spread(MutationType, Count) %>% 
  extract(Site, c("A","B"), "(^[A-Z])([0-9]*)") %>% 
  arrange(as.numeric(B)) %>%
  unite("A", "B", col = "Site", sep = "")
```
Exercise: Create a pretty table with the `kable` function and `kableExtra`. Add titles and captions.

```{r}
mutation_count_df %>% gather(Site, Count, P1:P99) %>% 
  ggplot(aes(x = Site)) + geom_point(aes(y = Count, color = MutationType), size = .5) + 
  scale_x_discrete(name ="Site", 
                    breaks=seq(from = 0, to = 100, by = 10), labels= as.character(seq(from = 0, to = 100, by = 10))) + 
  
  facet_wrap(vars(MutationType))
```

Let's return to the original data frame and explore the relationship between drug resistance and mutations.

Exercise: For site P10, plot resistance scores for each of the protease inhibitors by mutation. Hint: This requires a gather operation.

```{r}
gene_df %>% gather(PI,ResistanceValue, APV:SQV) %>% ggplot(aes(x = P10)) +
  geom_point(aes(y = ResistanceValue, color = PI)) +
  theme(axis.text.x = element_text( 
                           size=10, angle=80)) +
  
  facet_wrap("PI", 5)
```

Additional Exercises: 

1. Plot the resistance values for P10 for each PI and for each mutation type (substitution, insertion, deletion, WT)

```{r}
gene_df_with_categories %>% gather(PI,ResistanceValue, APV:SQV) %>% ggplot(aes(x = P10)) +
  geom_point(aes(y = ResistanceValue, color = PI)) +
  theme(axis.text.x = element_text( 
                           size=10, angle=80)) +
  
  facet_wrap("PI", 5)
```

2.  Plot resistance values for APV by mutation type for sites 1 through 10.

```{r}
gene_df_with_categories %>% 
  dplyr::select(IsolateName:P10) %>%
  gather(Site, MutationType, P1:P10) %>%
  ggplot(aes(x = MutationType)) + 
  geom_point(aes(y = APV, color = Site)) +
  theme(axis.text.x = element_text( 
                           size=10, angle=80)) +
  
  facet_wrap("Site", 5)

```

3. Download the data for RT inhibitors into two separate data frames, one for the NRTIs and one for the NNRTIs. (Hint: go to the url we downloaded the PI data from.)

4. Classify the mutations as we did for protease inhibitors for each data set. Count the types of mutations by site for each data set.

Group Exercise: Break into groups of 2 or 3 and come up with questions about the data and design visualizations/representations to explore those questions.
