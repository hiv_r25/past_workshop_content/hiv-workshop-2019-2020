
# Quantitative Methods for HIV/AIDS Research - LME Workshop

## Preliminary Steps

### Load Packages
```{r}
library(tidyverse)
library(nlme)
library(RColorBrewer)
```

### Get sleep study data from lme4
```{r}
library(lme4)
sleepstudy <- as_tibble(sleepstudy)
detach("package:lme4")
```


```{r}
library(lme4)
sleepstudy <- as_tibble(sleepstudy)
detach("package:lme4")
```

### Set custom colors
```{r}
mypal3   <- brewer.pal(3,"Set1")
myred3   <- mypal3[1]
myblue3  <- mypal3[2]
mygreen3 <- mypal3[3]
```

## Rails Data

### Data illustration
```{r}
ggplot(aes(Rail, travel), data=Rail)+
    geom_point(col = myblue3)+
    theme_bw()+
    xlab("Rail Number")+
    ylab("Zero-force travel time (nano-seconds)")
```

### Random intercept analysis
```{r}
mod0<-summary(lm(travel~1, data = Rail))
mod0
```

### Simulation function
```{r}
sim.ranef <- function(nk, n, se, sb) {
    # n exp units with nk replicates each
    N <- n * nk
    # Draw N (big) measurement errors from Normal[0, se]
    e <- rnorm(N, 0, se)
    # Draw n (small) random effects from Normal[0, sb]
    b <- rnorm(n, 0, sb)
    # Each experimental unit has nk replicates
    b <- rep(b, each = nk)
    # Need id variable of form 1,1,1,2,2,2, ..., n, n, n,
    id <- rep(1:n, each = nk)
    # Now 
    y <- 0 + e + b
    # Create a data frame with columns id and y
    # Note that e and b are not observable so it does not make sense
    # to include in data object
    mydata <- data.frame(id, y)
    # Fit standard linear model and extract P-value
    mod0 <- summary(lm(y ~ 1, data = mydata))
    pval0 <- mod0$coef["(Intercept)", "Pr(>|t|)"]
    # Fit mixed-effects linear model and extract P-value
    mod1 <- summary(lme(y ~ 1, random = ~1 | id, data = mydata))
    pval1 <- mod1$tTable["(Intercept)","p-value"]
    # Return the two p-values
    return(c(pval0 = pval0, pval1 = pval1))
}
```

### Run simulation
```{r}
set.seed(21110)
B <- 10L
res <- as_tibble(t(replicate(B, sim.ranef(nk = 3, n = 6, se = 0.25, sb = 0.5))))
res %>% head(3)
res %>% mutate_all(~(. < 0.05)) %>% colMeans
```

Use larger sample size (n=50)

```{r}
res <- as_tibble(t(replicate(B, sim.ranef(nk = 3, n = 50, se = 0.25, sb = 0.5))))
res %>% head(3)
res %>% mutate_all(~(. < 0.05)) %>% colMeans
```

## Example 2: 

### Data illustration
```{r}
tibble(
    y = 2 + rep(rnorm(6, 0, 0.5), each = 3) + rnorm(6*3, 0, 0.25),
    id = rep(paste("cline", 1:6), each = 3),
    trt = "Control") %>%
    bind_rows(tibble(
        y = 4 + rep(rnorm(6, 0, 0.5), each = 3) + rnorm(6*3, 0, 0.25),
    id = rep(paste("cline", 1:6), each = 3),
    trt = "Treatment",
    )) %>%
    ggplot(aes(id, y, col = trt)) +
    geom_point()+
    theme_bw()+
    xlab("Cell Line")+
    ylab("Phenotype (unit)")+
    scale_colour_manual(values = c(myblue3, myred3))+
    geom_hline(yintercept = 4, color = myred3, linetype="dashed", size = 2)+
    geom_hline(yintercept = 2, color = myblue3, linetype="dashed", size = 2)
```

### Simulation fuction
```{r}
sim.twosample.example <- function(nk, n, se, sb) {
    # Each group will have nk replicates fron n experimental
    # units. So the number of observations (not experimental 
    # units is N = n*nk
    N <- n*nk
    # Simulate measurement errors for the first group
    e1 <- rnorm(N, 0, se)
    # Simulate measurement errors for the second group
    e2 <- rnorm(N, 0, se)
    # Simulate random effect for each of the n experimental
    # units in group 1
    b1 <- rnorm(n, 0, sb)
    # Replicate each of the n random effects nk times
    b1 <- rep(b1, each = nk)
    # Simulate random effect for each of the n experimental
    # units in group 2
    b2 <- rnorm(n, 0, sb)
    # Replicate each of the n random effects nk times
    b2 <- rep(b2, each = nk)
    # The N observed values for group 1
    y1 <- 0 + b1 + e1
    # The N observed values for group 1
    y2 <- 0 + b2 + e2
    # P-value from t-test ignoring the design
    t.test(y1, y2)$p.value
}
```

### Run simulation
```{r}
tibble(pval0 = replicate(B, sim.twosample.example(3,10,0.25,0))) %>%
    mutate(pval1 = replicate(B, sim.twosample.example(3,10,0.25,0.5))) ->
    res

res %>% mutate_all(~(. < 0.05)) %>% colMeans
```

## Example 3

### Generate data
```{r}
set.seed(115531)
tibble(id=as.character(rep(1:3,each=10)),
       Days=rep(0:9,3),
       bi=rep(rnorm(3,0,0.5), each=10),
       bs=rep(rnorm(3,0,0.5), each=10),
       e=rnorm(30, 0, 0.25)) -> mydf

```


### No random intercept slope

```{r}

mydf %>% mutate(y=3 + 0 + (0+0.5)*Days + e) %>%
    ggplot(aes(Days, y, group = id, col = id)) + 
    geom_line() + 
    theme_bw() +
    ylab("Phenotype (unit)") +
    scale_colour_manual(values =brewer.pal(3, "Set1"))+
    scale_x_discrete(limits=0:9)
    
```

### Random intercept/slope moidel with measurement errors

```{r}
mydf %>%  mutate(y=3 + bi + (bs+0.5)*Days + e) %>%
    ggplot(aes(Days, y, group = id, col = id)) + 
    geom_line() + 
    theme_bw() +
    ylab("Phenotype (unit)") +
    scale_colour_manual(values =brewer.pal(3, "Set1"))+
    scale_x_discrete(limits=0:9)
```

### Data analysis
```{r}
lme(Reaction ~ Days, random=~1+Days|Subject, data = sleepstudy)
```


```{r}
sessionInfo()
```
